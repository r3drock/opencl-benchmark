CC=gcc
CXX=g++

SRC_GPU=gpu.c
SRC_CPU=cpu.c
SRC_INIT=datainit.c
OUT_GPU=gpu
OUT_CPU=cpu
OUT_INIT=init

all: clean init gpu cpu

init:
	$(CC) $(SRC_INIT) -o $(OUT_INIT) -O3

gpu:
	$(CXX) $(SRC_GPU) -o $(OUT_GPU) -O3 -lOpenCL

cpu:
	$(CXX) $(SRC_CPU) -o $(OUT_CPU) -O3 -fopenmp

clean:
	rm -f $(OUT_INIT) $(OUT_GPU) $(OUT_CPU)
