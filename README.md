# OpenCL Performance Test

This is a simple test to compare the performance between CPU and GPU computation.

There are two program `cpu.c` and `gpu.c`, both of them will calculate the sum of all the numbers that can be written as the sum of fifth powers of their digits. In the end, the program will display the elapsed time.

## How to run

First, you need to compile the programs.

Type the following command to your terminal:

```
make
```

When the compiling finished, you will have 2 binary files: `cpu` and `gpu`.

Run test on CPU:

```
./cpu
```

Run test on GPU:

```
./gpu
```
