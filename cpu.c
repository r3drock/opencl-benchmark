#include <limits.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <iostream>
#include <ctime>
#include <ratio>
#include <chrono>

unsigned int pow(unsigned int  a, unsigned int b);

int main() {
    using namespace std::chrono;
    steady_clock::time_point t1 = steady_clock::now();
	const unsigned int SIZE = (INT_MAX/30);
	clock_t begin = clock();

	for (unsigned int j = 10;j < SIZE; j++) {  
		int sum = 0;

		for (int i = j; i > 0; i = i / 10) {
			int a = i % 10;
			sum += a * a * a * a * a;
		}
		if (j == sum)
			printf("%d %d\n",(int) j, (int) sum);
	}

	steady_clock::time_point t2 = steady_clock::now();

	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
	std::cout << "Runtime: " << time_span.count() << "s" << std::endl;
}
