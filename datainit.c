#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const size_t SIZE = 122 * 162;

int main() {
	//srand(time(NULL));
	float* data;
	data = (float*) calloc(SIZE, sizeof(data[0]));

	printf("%llu\n",SIZE);

	for (size_t i = 0; i < SIZE ; i++)
		data[i] = rand() % 256;

	FILE *fp;
	fp = fopen("data", "w");
	fwrite(data, sizeof(data[0]), SIZE, fp);
	fclose(fp);

	free(data);
}
