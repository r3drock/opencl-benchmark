#include <cstdio>
#include <iostream>

#include <ctime>
#include <ratio>
#include <chrono>

#include <climits>
#include <cstdlib>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_SOURCE_SIZE (0x100000)


int main() {
    using namespace std::chrono;
    steady_clock::time_point t1 = steady_clock::now();

	const unsigned int SIZE = ((INT_MAX/30) / 100) - 10;
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_platform_id platform_ids[100];
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;

	char * source_str;
	size_t source_size;

	FILE *fp;
	fp = fopen("kernel.c", "r");
	if (!fp) {
		fprintf(stderr, "Failed to load kernel\n");
		exit(1);
	}
	source_str = (char*)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	if (!source_str){
		fprintf(stderr, "source string is NULL\n");
		exit(1);
	}
	fclose(fp);



	ret = clGetPlatformIDs(0, 0, &ret_num_platforms);
	ret = clGetPlatformIDs(ret_num_platforms, platform_ids, NULL);

	ret = clGetDeviceIDs(platform_ids[0], CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);

	if (ret != CL_SUCCESS)
	{
		printf("Error: Failed to create a device group!\n");
		return -1;
	}

	context = clCreateContext(0, 1, &device_id, NULL, NULL, &ret);
	if (!context || ret != CL_SUCCESS)
	{
		printf("Error: Failed to create a compute context!\n");
		return -1;
	}

	command_queue = clCreateCommandQueue(context, device_id, 0, &ret);
	if (!command_queue || ret != CL_SUCCESS)
	{
		printf("Error: Failed to create a command commands!\n");
		return -1;
	}


	program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);
	if (!program || ret != CL_SUCCESS)
	{
		printf("Error: Failed to create compute program errorcode: %d!\n", ret);
		return EXIT_FAILURE;
	}

	ret = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	if (!program || ret != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel errorcode: %d!\n", ret);
		return EXIT_FAILURE;
	}

	kernel = clCreateKernel(program, "hello", &ret);
	if (ret != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel errorcode: %d!\n", ret);
		return -1;
	}
	if (!kernel )
	{
		printf("Error: Failed to create compute kernel!\n");
		return -1;
	}


    steady_clock::time_point t2 = steady_clock::now();

	size_t localWorkSize[1];
	localWorkSize[0] = 1;
	size_t globalWorkSize[1];
	globalWorkSize[0] = SIZE;
	printf("globalWorkSize: %zu \n", globalWorkSize[0]);

	unsigned int psum[1] = {0};
	cl_mem cl_psum = clCreateBuffer(context,
			CL_MEM_READ_WRITE |  CL_MEM_COPY_HOST_PTR |  CL_MEM_ALLOC_HOST_PTR, 
			sizeof(psum[0]), psum, &ret);
	if (cl_psum == NULL || ret != CL_SUCCESS) {
		printf("Error Status %d in clCreateBuffer.\n", ret);
		printf("memObjects are null.\n");
		exit(-1);
	}

	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&cl_psum);
	if (ret != CL_SUCCESS) {
		printf("Error setting Kernel Arguments.\n");
		exit(-1);
	}
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, 
			NULL, globalWorkSize,
			localWorkSize, 0, NULL,
			NULL);
	if (ret != 0)
	{
		printf("Error Status %d in clEnqueueNDRangeKernel.\n", ret);
		exit(-1);
	}

	ret = clEnqueueReadBuffer(command_queue, cl_psum,
			CL_TRUE, 0, sizeof(psum[0]), psum, 0, NULL, NULL);
	if (ret != 0)
	{
		printf("Error Status %d in clEnqueueReadBuffer.\n", ret);
		exit(-1);
	}



	ret = clFlush(command_queue);
	ret = clFinish(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);

	steady_clock::time_point t3 = steady_clock::now();

	duration<double> time_span_compilation = duration_cast<duration<double>>(t2 - t1);
	duration<double> time_span_execution = duration_cast<duration<double>>(t3 - t2);

	std::cout << "Sum: " << psum[0] << std::endl;
	std::cout << "Runtime compilation: " << time_span_compilation.count() << "s" << std::endl;
	std::cout << "Runtime execution: " << time_span_execution.count() << "s" << std::endl;
	return 0;
}
