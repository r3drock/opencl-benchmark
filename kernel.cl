__kernel void hello(__global uint * psum) {
	unsigned int j = get_global_id(0) + 10;
	int sum = 0;
	for (int i = j; i > 0; i = i / 10) {
		int a = i % 10;
		sum += a * a * a * a * a;
	}
	if (sum == j)
		atomic_add(psum, sum);
}
